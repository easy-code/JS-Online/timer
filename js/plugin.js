// Timer
const buttons = document.querySelectorAll('[data-time]');
const form = document.forms['customForm'];
const minutesInput = form.elements.minutes;
const stopBtn = document.querySelector('.stopBtn');
const pauseBtn = document.querySelector('.pauseBtn');
const controlBlock = document.querySelector('.control-block');

const timer = (function() {
    let countdown,
        timerDisplay,
        endTime,
        alarmSound,
        secondsLeft;

    let pause = false;

    // Function init timer
    function init(settings) {
        timerDisplay = document.querySelector(settings.timeLeftSelector);
        endTime = document.querySelector(settings.timerEndSelector);

        if (settings.alarmSound) {
            alarmSound = new Audio(settings.alarmSound);
        }

        return this;
    }

    // Function start timer
    function start(seconds) {
        if (!timerDisplay || !endTime) return console.log('Please init module first.');
        if (!seconds || typeof seconds !== 'number') return console.log('Please provide seconds.');

        resetTimer();

        const now = Date.now();
        const then = now + seconds * 1000; // milliseconds

        displayTimeLeft(seconds);
        displayEndTime(then);

        controlBlock.style.display = 'flex';
        pauseBtn.textContent = 'Pause';
        pause = false;

        countdown = setInterval(() => {
            secondsLeft = Math.round((then - Date.now()) / 1000);

            if (secondsLeft < 0) {
                clearInterval(countdown);
                playSound();
                return;
            }

            displayTimeLeft(secondsLeft);
        }, 1000)
    }

    // Function pause timer
    function pauseTimer() {
        if (!pause){
            clearInterval(countdown);
            pauseBtn.textContent = 'Continue';
            pause = true;
        } else {
            start(secondsLeft);
        }
    }

    // Stop timer
    function stop() {
        resetTimer();

        displayTimeLeft(0);
        controlBlock.style.display = 'none';
        endTime.textContent = '';
    }

    // Function reset timer
    function resetTimer () {
        clearInterval(countdown);

        if (alarmSound){
            alarmSound.pause();
            alarmSound.currentTime = 0;
        }
    }

    // Function output time
    function displayTimeLeft(seconds) {
        const days = Math.floor(seconds / (3600 * 24));
        const hours = Math.floor((seconds % (3600 * 24) / 3600));
        const minutes = Math.floor((seconds % 3600) / 60);
        const reminderSeconds = seconds % 60;
        let display;

        if (days){
            display = `${days <= 0 ? '' :
                days < 2 ? days +' day ' :
                    days + ' days '}${hours < 10 ? '0' :
                ''}${hours}:${minutes < 10 ? '0' :
                ''}${minutes}:${reminderSeconds < 10 ? '0' :
                ''}${reminderSeconds}`;
        } else {
            display = `${hours <= 0 ? '' :
                hours < 10 ? '0' + hours + ':' :
                    hours + ':'}${minutes < 10 ? '0' :
                ''}${minutes}:${reminderSeconds < 10 ? '0' :
                ''}${reminderSeconds}`;
        }

        document.title = display;
        timerDisplay.textContent = display;
    }

    // Function display left time
    function displayEndTime(timestamp) {
        const end = new Date(timestamp);
        const days = end.getDay();
        const weeks = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const hours = end.getHours();
        const minutes = end.getMinutes();

        endTime.textContent = `Be back in ${weeks[days]} at ${hours}:${minutes < 10 ? '0' : ''}${minutes}`
    }

    // Function play sound
    function playSound() {
        alarmSound.play()
    }

    return {
        init,
        start,
        stop,
        pauseTimer
    }
})();

// Init timer
timer.init({
    timeLeftSelector: '.display__time-left',
    timerEndSelector: '.display__end-time',
    alarmSound: 'audio/bell.mp3'
}).start(10);

// Start timer by click on time-control buttons
function startTimerByBtn() {
    const seconds = parseInt(this.dataset.time);
    timer.start(seconds);
}

// Start timer by click on Submit button
function startTimerBySubmit(e) {
    e.preventDefault();
    const seconds = parseInt(minutesInput.value) * 60;
    timer.start(seconds);
    form.reset();
}

buttons.forEach(btn => btn.addEventListener('click', startTimerByBtn));
form.addEventListener('submit', startTimerBySubmit);
stopBtn.addEventListener('click', () => timer.stop());
pauseBtn.addEventListener('click', () => timer.pauseTimer());
